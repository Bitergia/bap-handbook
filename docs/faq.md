## General

??? faq "How to contact support?"
    You can contact support by our portal and by email using the web portal
    ([support.Bitergia.com](https://support.bitergia.com)) or using
    email ([support@Bitergia.com](mailto:support@bitergia.com)).

    See more information [here](../subscription/support#Support)


## Identities

??? faq "How to add a new organization?"
    You can add a new organization via HatStall.

    See more information [here](../advanced/affiliations#adding-a-new-organization)

??? faq "How to remove an organization?"
    You can remove an organization via HatStall.

    See more information [here](../advanced/affiliations#removing-an-organization)


??? faq "How to assign an e-mail domain to an organization?"
    You can assign an e-mail domain to an organization via HatStall.

    See more information [here](../advanced/affiliations#adding-an-e-mail-domain-to-an-organization)

??? faq "How to enroll a person to an organization during a time interval?"
    You can enroll profiles via HatStall.

    See more information [here](../advanced/affiliations#enrolling-a-person-to-an-organization)


??? faq "How to prevent that affiliations removed from a contributor are restored?"
    You can do it via HatStall.

    See more information [here](../advanced/affiliations#preventing-that-affiliations-removed-from-a-contributor-are-restored)


??? faq "How to un-enroll a person from an organization?"
    You can un-enroll a person via HatStall.

    See more information [here](../advanced/affiliations#un-enrolling-a-person-from-an-organization)


??? faq "How to correctly enroll a person to different organizations during consecutive time periods avoiding overlapping dates?"
    You need to understand how dates work.

    See more information [here](../advanced/affiliations#contributor-activity-is-labeled-with-a-single-company-when-shehe-is-enrolled-with-more-companies)


??? faq "How to enroll a person to two organizations during the same time intervals?"
    Intervals should not overlap.

    See more information [here](../advanced/affiliations#contributor-activity-is-labeled-with-a-single-company-when-shehe-is-enrolled-with-more-companies)


??? faq "How to mark an identitiy as bot?"
    You can mark identities as bot via HatStall.

    See more information [here](../advanced/affiliations#marking-bots)


??? faq "How many types of matching do exist?"
    There are 4.

    See more information [here](../advanced/affiliations#types-of-matching)


??? faq "How long does it take to see the changes in HatStall reflected in the dashboard?"
    The time depends on the amount of data available in the data source

    See more information [here](../advanced/affiliations#changes-are-not-visible-in-the-metrics-dashboard)


??? faq "What is the organization JSON file?"
    The organization JSON file contains a list of organizations and their domains.

    See more information [here](../advanced/projects_json_file)


## Data sources management

??? faq "How to add a new repository?"
    You have to edit `projects.json` file.

    See more information [here](../advanced/projects_json_file)


??? faq "How to add labels to repositories?"
    You have to edit `projects.json` file.

    See more information [here](../advanced/projects_json_file)


??? faq "How to track private repositories on GitHub?"
    We need permission.

    See more information [here](../supported/github/#tracking-private-repositories)


??? faq "How to track private repositories on GitLab?"
    We need permission.

    See more information [here](../supported/gitlab/#tracking-private-repositories)


## Dashboards

??? faq "How to inspect an index from Kibiter?"
    On `Discover` section.

    See more information [here](../advanced/explore#inspecting-an-index-from-kibiter)


??? faq "How to share a dashboard?"
    You have to be logged in and click on `Share`.

    See more information [here](../reporting_and_sharing#sharing-a-dashboard)


??? faq "How to properly save a new visualization or dashboard?"
    You have to mark `Save as new visualization`.

    See more information [here](../custom_visualizations#saving-new-visualizations)


??? faq "How to edit a visualization?"
    You have to click on `Edit`, then the `gear` and then `Edit visualization`.

    See more information [here](../custom_visualizations#editing-visualizations)


??? faq "How to edit the name of a visualization?"
    You have to edit the visualization.

    See more information [here](../custom_visualizations#renaming-visualizations)


??? faq "How to create a visualization?"
    You have to click on `Edit` and then on `Create new`.

    See more information [here](../custom_visualizations#creating-visualizations)


??? faq "How to edit a dashboard?"
    You have to click on `Edit` to enter in edition mode.

    See more information [here](../custom_visualizations#editing-a-dashboard)


??? faq "How to create a dashboard?"
    You have to go to `Dashboard` section and perform editions on edition mode.

    See more information [here](../custom_visualizations#creating-dashboards)
