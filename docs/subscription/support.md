When you suscribe to Bitergia Analytics Platform you have access to the following:

## Support

You can contact the **Bitergia Support team** in two ways:

  - using the web portal ([support.Bitergia.com](https://support.bitergia.com)).
  - using email ([support@Bitergia.com](mailto:support@bitergia.com)).

![Support](../assets/images/subscriptions/support/support_link.png)

### Using web portal (recommended)

First of all, you need to have an account.

To create an account, you can [sign up](https://support.bitergia.com/support/signup) and
if your account is not validated, we will ask you for validation to your company main
contact for Bitergia.

You can also ask to your company main contact for Bitergia to create a support account
and we will create it for you (you will receive a confirmation link on your email to
set up your password).

Once you are logged in, you can
[submit a ticket](https://support.bitergia.com/support/tickets/new)
or [view](https://support.bitergia.com/support/tickets) your current tickets
and reply to them. You will see something like the following:

![Support](../assets/images/subscriptions/support/submit_a_ticket.png)

The rest of your team members will be able to see the tickets and provide feedback to
them.

![Support](../assets/images/subscriptions/support/ticket_list.png)

If you want any of your team members to get notifications from a ticket, as soon as it
is created add them to the notification list with the 'Add people' button.

#### Notifications

By default, the requester will be notified when a new reply is added to the ticket.
All the people of your company will be able to see and collaborate in your tickets.
If you want to add people to be notified, after the ticket is created you have to
click on "Add people" and they will be notified as well.

![Support](../assets/images/subscriptions/support/ticket_notifications.gif)

### Using email

Just send an email with your request to [support@Bitergia.com](mailto:support@bitergia.com).
It automatically creates a new ticket, which will be responded by
**Bitergia Support Team**. You will receive an email confirmation with the creation of
the ticket and once **Bitergia Support Team** replies you will receive an email response.


## Why are results sometimes inaccurate?

Kibana counts are approximated. It uses a **cardinality aggregations** algorithm to
count data, which prioritize performace over accuracy. These cardinality algorithms
are designed to handle big ammounts of data, offering results fast. For this reason,
there's always an error associated to these counts. Usually, you will find these
problems with **unique counts** (e.g. total count of commits).

You can learn more about this topic and the precision error in the
[cardinality aggregation section](https://opensearch.org/docs/latest/opensearch/metric-agg/#cardinality)
of the Kibana's documentation.


## Which are the checks performed at Bitergia?

The checks aim at granting the reliability and availability of the service as well as
the quality and freshness of the data. They are listed below.

  - [Zabbix](https://www.zabbix.com/), which is an open source network and application
    monitoring. We use it to make sure that your instance is up and running.
  - [Logstash](https://www.elastic.co/logstash), which is an open source tool for
    managing logs. We use it to track the working of the instance and easily inspect
    the logs.
  - [Kibiter](https://github.com/chaoss/grimoirelab-kibiter), which is a downstream of
    [Kibana](https://www.elastic.co/kibana), an open source tool for data visualization.

It is the tool used to create dashboards to visualize your project data. The
`Data Status` dashboard (included in your instance) provides information about the
date when the data was lastly collected and enriched.

  - [Jenkins](https://jenkins.io/), which is an open source automation server. We use it
    to execute jobs that monitor the trend and quality of the data, thus enabling the
    detection of data loss.

In case of specific requests (e.g., meetings and events), we perform manual checks
on the instance based on the customer needs.


## How does Bitergia handle feature requests?

All customer's feature requests are evaluated and classified as:

  - improvements for the whole platform.
  - ad-hoc developments (specific to single customers).

In the case of improvements, they are included to the Bitergia's roadmap with a given
priority, based on the priority of the other tasks part of the roadmap. The priority
can be influenced by the customer by sponsoring the development.

In the case of ad-hoc developments, the impact of the requested feature is assessed in
terms of feasibility, maintenability and complexity. If the assessment is negative, the
feature request is rejected and alternatives are proposed (when possible).
For positive assessments, an estimate (hours and price) is communicated to the customer.

