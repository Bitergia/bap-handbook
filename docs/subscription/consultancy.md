With Bitergia's consultancy team as your metrics department you have access to
our wide experience. We can work with you towards specific outcomes such as a
metric strategy handbook, custom reports that analyze a specific
situation or custom visualizations that help you to get insights from your data.

Our team is highly qualified and with expertise in areas such as:

  - _OSPOs_. An Open Source Program Office is a department within an
    organization, usually a company, but may as well be a university, a
    foundation or other organizations. Bitergia assists its customer’s OSPO’s
    in their decision making by providing advice, guidance and tools to answer
    their concerns, usually by measuring the open source projects they are
    interested in.
  - _ISPOs_ or InneSource Program Offices. InnerSource is the application of
    best open source software practices within the walls of a corporation. Its
    main purpose is to break down silos of knowledge and development, and be
    more effective in the development of new pieces of software across several
     geographical organizations. Bitergia has the expertise of helping big
    corporations in the creation of an ISPO, where we have been able to
    empower teams in the understanding and use of InnerSource practices as
    well as tools such as development analytics to measure the impact of the
    initiative.
  - _Open Source Foundations_. Many of the most successful Open Source projects
    have a foundation behind, they offer support from different levels to allow
    those projects to make a bigger impact in their community of users.
    Bitergia has been working with foundations for more than a decade. In this
    time we have provided to them insights about the status of the different
    communities and transparency to all the members of the foundation.
  - _Open Source Projects_. Bitergia is a company with Open Source (or FLOSS)
    rooted in its DNA. We create, maintain and reuse Open Source. Our customers
    interested in analyzing Open Source Projects will benefit from a team of
    people really familiar with the most common barriers such projects have.
